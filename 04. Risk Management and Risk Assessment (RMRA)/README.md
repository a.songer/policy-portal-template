| Current Version                | Version #.#                                     |
| ------------------------------ | ----------------------------------------------- |
| Creation Date                  | `YYYY-MM-DD`                                    |
| Role Responsible               | `Role`                                          |
| Last Approval Date             | `YYYY-MM-DD`                                    |
| Distribution of Document       | Disbursed to all employees of `<Company Name>`. |


This policy establishes the scope, objectives, and procedures of `<Company>` (`<Company Initial>`) information security risk management process. The risk management process is intended to support and protect the organization and its ability to fulfill its mission.

**Related Documents**
- [04-1. Risk Assessment Procedure](/04.%20Risk%20Management%20and%20Risk%20Assessment%20(RMRA)/04-1.%20Risk%20Assessment%20Procedure.md)
- [04-2. Risk Mitigation Procedure](/04.%20Risk%20Management%20and%20Risk%20Assessment%20(RMRA)/04-2.%20Risk%20Mitigation%20Procedure.md)


# Policy Statements

`<Company Initial>` policy requires that:

(a) A thorough risk assessment must be conducted to evaluate the potential threats and vulnerabilities to the confidentiality, integrity, and availability of sensitive, confidential and proprietary electronic information it stores, transmits, and/or processes.

(b) Risk assessments must be performed with any major change to `<Company Initial>`'s business or technical operations and/or supporting infrastructure, no less than once per year.

(c) Strategies shall be developed to mitigate or accept the risks identified in the risk assessment process.

(d) Maintain documentation of all risk assessment, risk management, and risk mitigation efforts for a minimum of seven years.

# Controls and Procedures

## Risk Management Objectives

#### Applicable Standards

| **NIST CSF** |
| :----------- |
| `ID.RM-3`    |
| `ID.RM-2`    |
| `ID.RA-4`    |
| `ID.RA-5`    |
| `ID.BE-3`    |
| `ID.BE-2`    |

`<Company Initial>` has established formal risk analysis and risk management processes to

- identify risks that may impact its business operations or the confidentiality, integrity and availability of its critical data; and
- reduce risk to an acceptable level by implementation of mitigation controls.

Unmitigated risk above the pre-defined acceptable level must be reviewed, approved and accepted by senior management.

### Acceptable Risk Levels

Risks that are either low impact or low probability, based on the scoring mechanism defined in risk assessment process, are generally considered acceptable.

All other risks must be individually reviewed and managed according to the risk management process.

## Risk Management Process

#### Applicable Standards

| **HIPAA**              | **SOC2** | **CSA**  | **PCI** | **NIST CSF** |
| :--------------------- | :------- | :------- | :------ | :----------- |
| `164.308(a)(1)(ii)(B)` | `CC2.2`  | `BCR-09` | `6.4`   | `ID.GV-4`    |
| `164.308(a)(8)`        | `CC3.1`  | `GRM-02` | `6.6`   | `ID.RM-1`    |
|                        | `CC3.2`  | `GRM-04` |         | `ID.RA-5`    |
|                        | `CC5.1`  | `GRM-10` |         | `ID.RA-6`    |
|                        | `CC5.2`  | `GRM-11` |         |              |
|                        | `CC9.1`  | `STA-06` |         |              |

Risk analysis and risk management are recognized as important components of `<Company Initial>`'s corporate compliance and information security program.

`<Company Initial>`'s risk management process is developed in accordance with the Risk Analysis and Risk Management implementation specifications within the Security Management standard and the evaluation standards set forth in the HIPAA Security Rule, 45 CFR 164.308(a)(1)(ii)(A), 164.308(a)(1)(ii)(B), 164.308(a)(1)(i), and 164.308(a)(8).

Risk assessments are done throughout product life cycles:

- Before the integration of new system technologies and before changes are made to `<Company Initial>` physical and technical safeguards; and (Note that these changes do not include routine updates to existing systems, deployments of new systems created based on previously configured systems, deployments of new Customers, or new code developed for operations and management of the `<Company Initial>` Platform)
- While making changes to `<Company Initial>` physical equipment and facilities that introduce new, untested configurations.

`<Company Initial>` performs periodic technical and non-technical assessments of the security rule requirements as well as in response to environmental or operational changes affecting the security of sensitive data.

`<Company Initial>` implements security measures sufficient to reduce risks and vulnerabilities to a reasonable and appropriate level to:

1. Ensure the confidentiality, integrity, and availability of all sensitive data `<Company Initial>` receives, maintains, processes, and/or transmits for its Customers;
2. Protect against any reasonably anticipated threats or hazards to the security or integrity of Customer data and/or sensitive data;
3. Protect against any reasonably anticipated uses or disclosures of Customer data and/or sensitive data that are not permitted or required; and
4. Ensure compliance by all workforce members.

In addition, `<Company Initial>` risk management process requires that:

1. Any risk remaining (residual) after other risk controls have been applied, requires sign off by the senior management and `<Company Initial>`'s Security Officer.
2. All `<Company Initial>` workforce members are expected to fully cooperate with all persons charged with doing risk management work, including contractors and audit personnel. Any workforce member that violates this policy will be subject to disciplinary action based on the severity of the violation, as outlined in the `<Company Initial>` Roles Policy.
3. The implementation, execution, and maintenance of the information security risk analysis and risk management process is the responsibility of `<Company Initial>`'s Security Officer (or other designated employee), and the identified Risk Management Team.
4. All risk management efforts, including decisions made on what controls to put in place as well as those to not put into place, are documented and the documentation is maintained for six years.
5. The details of the Risk Management Process, including risk assessment, discovery, and mitigation, are outlined in detail below. The process is tracked, measured, and monitored using the following procedures:
   1. The Security Officer or the Privacy Officer initiates the Risk Management Procedures by creating an Issue in the Eramba GRC tool.
   2. The Security Officer or the Privacy Officer is assigned to carry out the Risk Management Procedures.
   3. All findings are documented and linked to the Issue.
   4. Once the Risk Assessment steps are complete, along with corresponding documentation, the Security Officer approves or rejects the Issue. If the Issue is rejected, it goes back for further review and documentation.
   5. If the review is approved, the Security Officer then marks the Issue as Done, adding any pertinent notes required.
6. The Risk Management Procedure is monitored on a quarterly basis using Eramba reporting to assess compliance with above policy.

Third party risk management details including procurement and systems acquisition can be found in §vendor.

### Risk Management Schedule

The two principle components of the risk management process - risk assessment and risk mitigation - will be carried out according to the following schedule to ensure the continued adequacy and continuous improvement of `<Company Initial>`'s information security program:

- Scheduled Basis - an overall risk assessment of `<Company Initial>`'s information system infrastructure will be conducted annually. The assessment process should be completed in a timely fashion so that risk mitigation strategies can be determined and included in the corporate budgeting process.
- Throughout a System's Development Life Cycle - from the time that a need for a new, untested information system configuration and/or application is identified through the time it is disposed of, ongoing assessments of the potential threats to a system and its vulnerabilities should be undertaken as a part of the maintenance of the system.
- As Needed - the Security Officer (or other designated employee) or Risk Management Team may call for a full or partial risk assessment in response to changes in business strategies, information technology, information sensitivity, threats, legal liabilities, or other significant factors that affect `<Company Initial>`'s Platform.

## Risk Assessment and Analysis

#### Applicable Standards

| **HIPAA**              | **SOC2** | **CSA CMM v4** | **PCI** | **NIST CSF** | **ISO 27001** | **ISO27002:2022** | **CIS CSC v8.0** | **EU GDPR** |
| :--------------------- | :------- | :------------- | :------ | :----------- | :------------ | :---------------- | :--------------- | :---------- |
| `164.308(a)(1)(ii)(A)` | `CC2.1`  | `A&A-06`       | `6.1`   | `ID.RA-1`    | `8.2`         | `5.8`             | 16.6             | `ART 35.1`  |
|                        | `CC3.1`  | `CEK-07`       | `9.6.1` | `ID.RA-3`    | `8.3`         | `8.12`            | 16.6             |             |
|                        | `CC3.2`  | `TVM-08`       |         | `ID.RA-4`    |               | `8.25`            |                  |             |
|                        | `CC5.1`  | `TVM-09`       |         | `ID.RA-6`    |               | `8.29`            |                  |             |
|                        | `CC5.2`  | `BCR-02`       |         |              |               | `8.30`            |                  |             |
|                        | `CC9.1`  |                |         |              |               |                   |                  |             |

The intent of completing a risk assessment is to determine potential threats and vulnerabilities and the likelihood and impact should they occur. The output of this process helps to identify appropriate controls for reducing or eliminating risk.

 

## Risk Mitigation and Monitoring

#### Applicable Standards

| **HIPAA**              | **SOC2** | **CSA**  | **PCI** | **NIST CSF** | **ISO 27001** | **ISO27002:2022** | **CIS CSC v8.0** |
| :--------------------- | :------- | :------- | :------ | :----------- | :------------ | :---------------- | :--------------- |
| `164.308(a)(1)(ii)(A)` | `CC2.1`  | `CEK-07` | `12.2`  | `ID.RA-6`    | `6.1.2`       | `7.5`             | 16.6             |
| `164.308(a)(8)`        | `CC3.1`  | `GRC-02` |         |              | `8.2`         | `5.8`             |                  |
|                        | `CC3.2`  |          |         |              |               |                   |                  |
|                        | `CC5.1`  |          |         |              |               |                   |                  |
|                        | `CC5.2`  |          |         |              |               |                   |                  |
|                        | `CC9.1`  |          |         |              |               |                   |                  |

Risk mitigation involves prioritizing, evaluating, and implementing the appropriate risk-reducing controls recommended from the Risk Assessment process to ensure the confidentiality, integrity and availability of `<Company Initial>` Platform data. Determination of appropriate controls to reduce risk is dependent upon the risk tolerance of the organization consistent with its goals and mission.

## Risk Registry

#### Applicable Standards

| **HIPAA**              | **SOC2** | **CSA**  | **NIST CSF** | **ISO27002:2022** | **CIS CSC** |
| :--------------------- | :------- | :------- | :----------- | :---------------- | :---------- |
| `164.308(a)(1)(ii)(A)` | `CC3.1`  | `STA-07` | `ID.RA-1`    | `5.19`            | 15.3        |
| `164.308(a)(8)`        | `CC5.1`  | `IAM-11` | `ID.RA-3`    | `5.21`            |             |
|                        | `CC5.2`  | `STA-02` | `ID.RA-6`    | `5.22`            |             |
|                        | `CC9.1`  | `STA-03` |              | `8.30`            |             |
|                        |          | `STA-13` |              |                   |             |
|                        |          |          |              |                   |             |

`<Company Initial>` Security team maintains a registry of risks, captured and kept updated

- in a document on company SharePoint; and/or
- in the security operations tool/database.

The risk registry includes all risks and threats identified during annual risk assessment and all interim reviews.

## Cyber Liability Insurance

#### Applicable Standards

| **SOC2** | **CSA**  | **ISO27002:2022** | **CIS CSC v8.0** |
| :------- | :------- | :---------------- | :--------------- |
| `CC9.1`  | `STA-13` | `5.19`            | `15.4`           |
|          |          | `5.20`            |                  |

`<Company Initial>` holds cyber liability insurance with sufficient coverage based on the organization's risk profile.

Our current cyber policy is covered by .

## Fraud Risks

#### Applicable Standards

| **SOC2** | **CSA**  | **PCI**  | **ISO27002:2022** | **CIS CSC v8.0** |
| :------- | :------- | :------- | :---------------- | :--------------- |
| `CC3.3`  | `IAM-11` | `12.8.2` | `15.9`            | `15.4`           |
| `CC3.4`  | `STA-02` | `12.8.4` | `8.30`            | `15.5`           |
|          | `UEM-14` |          |                   |                  |

Due to its transparent culture, team size and operating model, including separation of duties, comprehensive controls, continuous monitoring and auditing, `<Company Initial>` considers its fraud-related risk to be very low.

`<Company Initial>` hires to perform accounting services and annual financial audits.

Fraud risk is re-evaluated as part of the organization's annual risk assessment. The assessment considers the following aspects of fraud:

- Pressures and/or incentives
- Opportunities
- Rationalities

Financial-related fraud assessment is led by the COO/CFO.

IT-related fraud assessment is led by the Compliance Officer or CISO.

### Potential Frauds and Likelihood

| Fraud Risk                       | Likelihood | In Place Controls/Monitors                                   |
| :------------------------------- | :--------- | :----------------------------------------------------------- |
| Fraudulent Financial Reporting   | Low        | Monthly executive team reviews of business plan and revenue; Financial review by external accounting firm |
| Misappropriation of Assets       | Low        | Expense reporting and asset tracking in place                |
| Regulatory and Legal Misconduct  | Low        | Audit and compliance policies and processes, including whistleblower procedures; engage external law firm to review legal conduct |
| Payroll Fraud                    | Low        | Payroll is reviewed by at least two people internally as well as by external accounting firm |
| Kickbacks / Conflict of Interest | Low        | Team-based vendor review and selection process               |
| Misuse of Cloud Resources        | Low        | Continuous resource monitoring for all cloud accounts and regions and expense monitoring |
| Other IT Fraud                   | Low        | IT assets and resources tracking                             |





## Change History

| Date         | V    | Created by                                         | Description of change                                        |
| ------------ | ---- | -------------------------------------------------- | ------------------------------------------------------------ |
| YYYY-MM-DD   | #.#  | Contibutor                                         |                                                              |

