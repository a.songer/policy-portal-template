| Current Version          | Version #.#                                     |
| ------------------------ | ----------------------------------------------- |
| Creation Date            | `YYYY-MM-DD`                                    |
| Role Responsible         | `Role`                                          |
| Last Approval Date       | `YYYY-MM-DD`                                    |
| Distribution of Document | Disbursed to all employees of `<Company Name>`. |



`<Company>` (`<Short Name>`) is committed to protecting its employees, partners, clients/customers and the company itself from damaging acts either malicious or unintentional in nature. This includes implementation of policies, standards, controls and procedures to ensure the Confidentiality, Integrity, and  Availability of systems and data according to their risk level.
 

**The `<Short Name>` security program and policies are developed on the principles that** 

(1) security is everyone's responsibility and 

(2) self-management is best encouraged by rewarding the right behaviors.

# Controls and Procedures

## Information Security Program and Scope

### Applicable Standards

| **SOC2** | **NIST CSF** | **ISO27002:2013** |
| :------- | :----------- | :---------------- |
| `CC1.3`  | `ID.GV-1`    | `7.2.1`           |
| `CC5.3`  | `ID.BE-3`    | `5.1.1`           |

`<Short Name>` has developed a security program and implemented controls to meet and exceed all compliance requirements, including but not limited to ISO 27001, HIPAA, SOC 2 Common Criteria and other applicable industry best practices.

**On a high level, `<Short Name>`’s information security program covers:**

1. Inventory and protection of all critical assets
2. Visibility into and the management of data lifecycle, from creation to retention to deletion
3. Protection of data-at-rest, data-in-transit, and data-in-use
4. Segmented network architecture
5. Automated security configuration and remediation
6. Centralized identity and access management
7. Secure product development
8. Continuous monitoring and auditing
9. Validated plan and practice for business continuity, disaster recovery, and emergency response
10. End-user computing protection and awareness training

 

The information security program and its policies and procedures cover all `<Short Name>` workforce members, including full-time and part-time employees in all job roles, temporary staff, contractors and subcontractors, volunteers, interns, managers, executives employees, and third parties. The information security program is managed by dedicated security and compliance personnel, using Eramba as a GRC platform.

### Understanding the Policies and Documents

Policies are written in individual documents, each pertaining to a specific domain of concern.

Each document starts with the current version number and/or last updated date, followed by a brief summary. The remaining of the document is structured to contain two main sections:

- Policy Statements
- Controls and Procedures

All policy documents are maintained, reviewed, updated and approved following standards and procedures outlined in [Policy Management]().

### Review and Reporting

The information security program, policies, procedures and controls are reviewed on a regular basis internally by cross functional team members and externally by qualified assessors.

 

 

## Corporate Governance

`<Company>` believes in transparent and ethical business practices, and the protection of long-term interests of its employees, customers, shareholders and other stakeholders. 

`<Company>` has established a Board of Directors (Bod) and appointed qualified members and directors, such that: 

- A corporate bylaws and BoD charters are in place that describe board members responsibilities. 
- The BoD identifies and accepts its oversight responsibilities in relation to established requirements and expectations. 
- Board members are evaluated on a periodic basis to help ensure their skills and expertise are suited to lead senior management and take commensurate action. 
- The BoD has sufficient members who are independent from management and are objective in evaluations and decision making. 
- The expectations of the BoD and/or senior management are defined and understood at all levels of the organization and its service providers and business partners.

 

### Board of Directors Responsibilities 

The Board of Directors (BoD) meets quarterly to discuss financials, operations, business results, strategies and planning. The BoD responsibilities include: 

- Evaluate the performance of the Chief Executive Officer (CEO) and the executive management team 
- Establish policies, evaluate and approve the compensation of senior management of the company 
- Review succession plans and development programs for senior management 
- Review and approve long-term strategic and business plans and monitor organization's performance against the plans 
- Review and approve any major risks and the risk remediation/acceptance 
- Adopt policies of corporate conduct, including compliance with applicable laws, rules and regulations, maintenance of accounting, financial and other controls, and reviewing the adequacy of compliance systems and controls 
- Evaluate the overall effectiveness of the Board and its committees and the individual directors on a periodic basis 
- Adopt and implement best practices of corporate governance in full conformity with the letter and spirit of all applicable laws, rules and regulations

 

## Change History



| Date         | V       | Created by   | Description of change |
| ------------ | ------- | ------------ | --------------------- |
| `YYYY-MM-DD` | ``#.#`` | `Contibutor` |                       |


