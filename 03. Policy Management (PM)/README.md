| Current Version                | Version #.#                                     |
| ------------------------------ | ----------------------------------------------- |
| Creation Date                  | `YYYY-MM-DD`                                    |
| Role Responsible               | `Role`                                          |
| Last Approval Date             | `YYYY-MM-DD`                                    |
| Distribution of Document       | Disbursed to all employees of `<Company Name>`. |



`<Company Name>` (`<Company Initials>`) implements policies and procedures to maintain compliance and integrity of data. The Security Officer and Privacy Officer are responsible for maintaining policies and procedures and assuring all `<Company Initials>` workforce members, business associates, customers, and partners are adherent to all applicable policies. Previous versions of policies are retained to assure ease of finding policies at specific historic dates in time.

 



# Policy Statements

`<Company Initials>` policy requires that:

**(a)** `<Company Initials>` policies must be developed and maintained to meet all applicable compliance requirements adhere to security best practices, including but not limited to:

- HIPAA
- SOC 
- ISO 27002

**(b)** All policies must be reviewed at least annually.

**(c)** All policy changes must be approved by `<Company Initials>` Security Officer. Additionally,

- Major changes may require approval by `<Company Initials>` CEO or designee;
- Changes to policies and procedures related to product development may require approval by the Head of Engineering.

**(d)** All policy documents must be maintained with version control, and previous versions must be retained for a minimum of seven years.

**(e)** Policy exceptions are handled on a case-by-case basis.

- All exceptions must be fully documented with business purpose and reasons why the policy requirement cannot be met.
- All policy exceptions must be approved by both `<Company Initials>` Security Officer and COO.
- An exception must have an expiration date no longer than one year from date of exception approval and it must be reviewed and re-evaluated on or before the expiration date.

# Controls and Procedures

## Policy Management Process

| **HIPAA**            | **SOC2** | **CSA CCM v4** | **PCI**  | **NIST CSF** | **ISO 27001** | **ISO27002:2013** | **ISO27002:2022** |
| :------------------- | :------- | :------------- | :------- | :----------- | :------------ | :---------------- | :---------------- |
| `164.308(a)(1)(i)`   | `CC2.2`  | `A&A-01`       | `12.1`   | `RC.IM-2`    | `4.3`         | `5.1.1`           | `5.1`             |
| `164.308(a)(8)`      | `CC5.1`  | `AIS-01`       | `12.1.1` |              | `5.2`         | `5.1.2`           | `5.37`            |
| `164.316(a)`         | `CC5.3`  | `BCR-01`       |          |              | `6.1.1`       | `12.1.1`          |                   |
| `164.316(b)(1)`      |          | `CCC-01`       |          |              | `7.4`         |                   |                   |
| `164.316(b)(1)(i)`   |          | `CEK-01`       |          |              | `7.5.1`       |                   |                   |
| `164.316(b)(1)(ii)`  |          | `DCS-01`       |          |              | `7.5.2`       |                   |                   |
| `164.316(b)(1)(iii)` |          | `DCS-02`       |          |              | `7.5.3`       |                   |                   |
|                      |          | `DCS-03`       |          |              |               |                   |                   |
|                      |          | `DCS-04`       |          |              |               |                   |                   |
|                      |          | `DSP-01`       |          |              |               |                   |                   |
|                      |          | `GRC-01`       |          |              |               |                   |                   |
|                      |          | `GRC-02`       |          |              |               |                   |                   |

#### Applicable Standards



**IMPORTANT**

- Do not delete content directly, anything that needs to be deleted, please use the strikethrough first.  
- If you are going to add content directly, please make the text in blue. Adding new content means the text is in draft form.
- If there is a headline or text that is in this **color**. The additional content needs to be added.
- When you delete (or replace) content, then highlight in red. 

### Document Structure 

Policies are written in individual documents, each pertaining to a specific domain of concern.

Each document starts with the current version number in the format of `#.#` (e.g. `2.1`), followed by a brief summary. The remaining of the document is structured to contain the following subsections:

- Policy Statements
- Applicable Standards
- Controls and Procedures

### Versioning

Each `<Company Initials>` policy document contains a version and optionally a revision number. The version number is the four digit year followed by a number, to indicate the year and sequence number of the policy at which time it was written or updated.

The version number shall be incremented by one with each material change to the policy content. For example, if a new policy statement is added or a technical control/procedure is updated to ``2.1`` version of a policy, the new version should be numbered ``2.1``.

The policy document may also include a revision number, in the format of `rev.V#`, immediately following the main version number. A revision number indicate minor, non-material changes to the document, such as formatting changes, fixing typos, or adding minor details.

### Numbering

If sequencing numbers are included in the policy headings:

- Policy may be referenced by its statement number, such as `§2.1(a)`, in internal/external communications as well as in other `<Company Initials>` policies or technical/business documentation for cross reference.
- As such, to maintain cross referencing integrity, starting from version ``2.1``, all numbering shall remain intact for policy documents and statements.
- When updating, avoid reordering and renumbering of policy documents and statements. For example:
  - Append at the end of the list by adding new statement(s) as needed instead of inserting.
  - If a policy or policy statement is no longer applicable, mark it deprecated instead of removing the file or statement completely.

### Review and Maintenance of Policies

1. All policies are stored and up to date to maintain `<Company Initials>` compliance with ISO, HIPAA, SOC 2 and other relevant standards. Updates and version control are done similar to source code control.
2. Policy update requests can be made by any workforce member at any time. Furthermore, all policies are reviewed annually by the Security and Privacy Officer to assure they are accurate and up-to-date.
3. `<Company Initials>` employees may request changes to policies using the following process:
   1. The `<Company Initials>` employee initiates a policy change request by creating an Issue in the FSD project. The change request may optionally include a pull request from a separate branch or repository containing the desired changes. Highlight section in policy and leave a comment regarding change that is needed.
   2. The Security Officer or the Privacy Officer is assigned to review the policy change request. comment and make necessary change if it is warranted.
   3. Once the review is completed, the Security Officer approves or rejects the Issue. If the Issue is rejected, it goes back for further review and documentation.
   4. If the review is approved, the Security Officer then marks the Issue as Done, adding any pertinent notes required.
   5. If the policy change requires technical modifications to production systems, those changes are carried out by authorized personnel using `<Company Initials>`'s change management process.
   6. If the change results in a new version instead of a new revision (see §3.3.1 for definitions), the current version of the policy document(s) must be saved to archive under the corresponding version number prior to the new policy being adopted/published and prior to merging the pull request containing the changes. This allows easy reference to previous versions if necessary.
4. All policies are made accessible to all `<Company Initials>` workforce members. The current master policies are published at [Policy Portal]().
   - Changes are automatically communicated to all `<Company Initials>` team members through integrations between and #Security Slack that log changes to a predefined `<Company Initials>` Slack Channel.
   - The Security Officer also communicates policy changes to all employees via email. These emails include a high-level description of the policy change using terminology appropriate for the target audience.
5. All policies, and associated documentation, are retained for 7 years from the date of its creation or the date when it last was in effect, whichever is later
6. The policies and information security policies are reviewed and audited annually, or after significant changes occur to `<Company Initials>`'s organizational environment, by the security committee members. Issues that come up as part of this process are reviewed by `<Company Initials>` management to assure all risks and potential gaps are mitigated and/or fully addressed. The process for reviewing policies is outlined below:
   1. The Security Officer initiates the policy review by creating an Issue in the FSD project or via a issue. in Eramba.
   2. The Security Committee members and additional reviewers are notified by email or via the issue to review the current policies.
   3. If changes are made, the above process is used. All changes are documented in the Issue.
   4. Once the review is completed, the Security Officer approves or rejects the Eramba Issue. If the Issue is rejected, it goes back for further review and documentation.
   5. If the review is approved, the Security Officer then marks the Issue as Done.
   6. Policy review is monitored using Github Eramba or reporting to assess compliance with above policy.

 

Additional documentation related to maintenance of policies is outlined in [Roles and Responsibilities](/02.%20Roles%2C%20Responsibilities%20and%20Training%20(RRT)/README.md).


## Change History

| Date         | V    | Created by                                         | Description of change                                        |
| ------------ | ---- | -------------------------------------------------- | ------------------------------------------------------------ |
| YYYY-MM-DD   | #.#  | Contibutor                                         |                                                              |

